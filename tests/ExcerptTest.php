<?php
namespace App\Tests;

use PHPUnit\Framework\TestCase;
use App\Entity\Post;

class ExcerptTest extends TestCase
{
    public function testExcerptCanBeExtracted()
    {
        $post = new Post();
        $post->setBody('hello');
        $this->assertEquals('hello', $post->getExcerpt(40));
    }

    public function testRedactedExcerptEndsWithEllipsis()
    {
        $post = new Post();
        //              123456789012345678901234567890123456789012345
        $post->setBody('hellohellohellohellohellohellohellohellohello');
        $this->assertEquals('hellohellohellohellohellohellohellohello…', $post->getExcerpt(40));
    }

    public function testExactExcerptsLengthHasNoEllipsis()
    {
        $post = new Post();
        //              1234567890123456789012345678901234567890
        $post->setBody('hellohellohellohellohellohellohellohello');
        $this->assertEquals('hellohellohellohellohellohellohellohello', $post->getExcerpt(40));
    }

    public function testMultibyteCharactersAreTreatedAsSingleCharacters()
    {
        $post = new Post();
        //              1234567890123456789012345678901234567890
        $post->setBody('छछछछछछछछछछछछछछछछछछछछछछछछछछछछछछछछछछछछछछछछछ');
        $this->assertEquals('छछछछछछछछछछछछछछछछछछछछछछछछछछछछछछछछछछछछछछछछ…', $post->getExcerpt(40));
    }
}
