Happy blog
==========

Happy blog is a Symfony 4 example project, masquerading as a lightweight blogging tool.

Getting started
---------------

First off, you'll have to create the database we'll need by yourself.

    mysql -uuser -ppassword <<< "CREATE DATABASE happyblog;"

Edit ``.env`` so that the ``DATABASE_URL`` variable matches your current configurations.

    cp .env.dist .env
    vim .env

Let Composer do its magic.

    composer install

Make sure all tests pass.

    bin/phpunit

It's time to create the structure for, and populate, the database.

    bin/console doctrine:migrations:migrate
    mysql -uuser -ppassword happyblog < exampleseed.sql

That covers everything. You can spin up a temporary web server with PHP.

    php -S localhost:8080 -t public

Surf to http://localhost:8080/ using your favorite browser.
