DELETE FROM `comment`;
DELETE FROM `post`;

--1
INSERT INTO `post` (`id`, `heading`, `created_at`, `body`) VALUES (1, 'Dolor sit amet', '2014-06-11', 'Optio similique dolores maiores atque optio. Est libero maiores vel quis sint est iste molestias. Non ea non a sed quisquam nostrum id et. Unde eum magni reiciendis beatae earum optio. Tempore unde dolore quo quos cupiditate facilis illo. Error et numquam error temporibus in.
Porro iure cumque nihil dolor inventore quia et quia. Itaque quisquam aliquam similique tempora iste saepe explicabo ea. Velit voluptas repellendus sit necessitatibus aut sunt debitis.
Ea ex fuga praesentium esse. Qui magni aut qui et aut nisi quia. Qui non qui repudiandae nostrum consequuntur ratione. Corrupti saepe alias exercitationem.');
--2
INSERT INTO `post` (`id`, `heading`, `created_at`, `body`) VALUES (2, 'Qui id esse', '2014-06-13', 'Recusandae molestiae sunt et aliquid aut quibusdam cupiditate delectus. Rerum optio consequuntur voluptate exercitationem odio. Necessitatibus ullam sit mollitia iure nostrum. Autem non sapiente nostrum quo quasi ut voluptatem sint. Possimus molestias commodi dolorum quidem. Sed est quam eligendi.
Qui id esse ea dolores et perspiciatis. Quos magnam tempore explicabo vero explicabo et. Eius expedita deserunt enim officiis. Rerum nihil nam molestiae. Tenetur ex in sit sit assumenda delectus accusamus asperiores.');
--3
INSERT INTO `post` (`id`, `heading`, `created_at`, `body`) VALUES (3, 'Autem consequatur!', '2014-06-18', 'Deserunt aut suscipit quae. Et eos placeat aspernatur quia ab. Adipisci explicabo nulla voluptas saepe. Error numquam repellat possimus quia.
Labore voluptas et sit accusantium suscipit. Asperiores architecto debitis iure fugiat. Quia molestias autem consequatur et. Id nisi qui ipsa.
Omnis consequuntur delectus accusamus illum. Earum officiis et reiciendis repellat error. Officiis nobis aliquid iste consequatur voluptatem laborum.
Natus numquam natus ab. Aliquam in nihil adipisci fugiat. Eligendi dolores corporis facilis qui rem quos sed illum.');
--4
INSERT INTO `post` (`id`, `heading`, `created_at`, `body`) VALUES (4, 'Modi culpa…', '2014-06-19', 'Sed iusto labore atque nesciunt tempore dolor sapiente est. Tenetur architecto et ad ipsam dolor sunt illum. Aliquam earum accusamus sequi et facere laboriosam. Voluptatem et dicta itaque numquam. Architecto mollitia aut recusandae aperiam quaerat. Est porro quasi dolore est nihil voluptatem voluptatum commodi.
Porro eos veritatis harum officiis quia. Praesentium minus libero laboriosam ut. Unde recusandae officiis velit cupiditate. Nihil cum incidunt iste qui ex modi culpa laudantium.');
--5
INSERT INTO `post` (`id`, `heading`, `created_at`, `body`) VALUES (5, 'Repellat quasi?', '2014-06-23', 'Possimus amet nihil tempore tempore. Et ipsum consequatur quasi ut et est. Ut similique repellat repellat. Ut quaerat alias illum.');
--6
INSERT INTO `post` (`id`, `heading`, `created_at`, `body`) VALUES (6, 'Expedita ex et occaecati!', '2014-06-29', 'Iure qui aperiam sequi voluptate officiis nesciunt nobis maiores. Est nesciunt aliquid hic non. Praesentium et ad aut rerum et a voluptates in. Nisi earum neque est maiores consequuntur deleniti. Enim dolores voluptatem suscipit.
Rerum pariatur laboriosam ut illum. Nesciunt aspernatur sint iste. Et occaecati sequi ut culpa nam qui sit. Expedita quia qui ex molestias minus.
Iure sunt delectus consequatur odio. Quasi sed dolor voluptatem est molestiae et. Quidem eos molestiae veritatis sunt.');
--7
INSERT INTO `post` (`id`, `heading`, `created_at`, `body`) VALUES (7, 'Quas officiis sed quibusdam id…', '2014-07-03', 'Nihil et placeat sapiente adipisci voluptas voluptatem. Consequatur consequatur quia expedita. Commodi id officiis aspernatur sed quo. Omnis non id illum.
Alias deleniti vel architecto sit corporis voluptas incidunt rem. Quas officiis sed quibusdam id praesentium. Sunt qui sit soluta ut sed veritatis aut architecto. Ipsum non maiores voluptatibus et blanditiis.');
--8
INSERT INTO `post` (`id`, `heading`, `created_at`, `body`) VALUES (8, 'Sed quas quia', '2014-07-04', 'Et enim animi quas ab cum. Dignissimos unde autem laboriosam voluptatibus vel enim voluptas voluptatem. Ut mollitia cumque quia id ipsam sunt. Quis quidem laborum modi ut maiores officia. Et totam quo expedita qui sit nemo omnis voluptas.
Explicabo temporibus placeat reiciendis quia doloribus placeat aut architecto. Ut et eum quisquam et neque illo ut. Dolor at eveniet aliquid officia doloremque sed quas quia. Hic consequatur qui ex quia vel aut delectus culpa. Maxime vitae alias itaque et pariatur id sunt ut.');
--9
INSERT INTO `post` (`id`, `heading`, `created_at`, `body`) VALUES (9, 'Aspernatur: facilis!', '2014-07-05', 'Voluptatem nam eum delectus pariatur reiciendis. Aspernatur odio fugiat et. Ut facilis temporibus vel et praesentium nisi et illum. Et velit et quo laborum quisquam culpa. Vero omnis id aut eaque.
Voluptas voluptatem eligendi quam tenetur rerum aperiam sint. Qui dolores excepturi aliquid perspiciatis repellendus culpa repudiandae beatae. Voluptas ratione omnis excepturi et voluptates. Quae tempore et voluptatem adipisci expedita qui voluptas aut. Aliquam illum dolores aut cupiditate rerum vero dolor. Nemo aliquid et quia sit placeat.');
--10
INSERT INTO `post` (`id`, `heading`, `created_at`, `body`) VALUES (10, 'Dolorum nihil rerum', '2014-07-11', 'Veritatis doloribus qui neque consequatur ea. Fuga at qui error minus. Dolores assumenda deserunt cumque ea.
Omnis est reiciendis sed itaque. Quia eveniet vitae qui dolores quas. Ut quos vero qui odio. Dolorum nihil rerum quos dolor dolor earum eius maiores. Quasi eveniet eos officiis pariatur aut est sequi fugiat. Fugit consequuntur repellat distinctio sunt.
Reiciendis aut assumenda est placeat in. Quo dolor eligendi et. Odio officiis dolor sunt est. Culpa est sequi pariatur est rem odit ut. Voluptatem laboriosam sapiente nobis ducimus voluptate blanditiis.
Eaque quia quasi accusantium eum nesciunt aut aut exercitationem. Consequatur dolores quisquam nulla ut quia. Perspiciatis illo qui fuga. Praesentium aliquam ea voluptatem in corrupti molestiae. Animi veritatis dolor occaecati reprehenderit vel assumenda vel. Qui eveniet repudiandae et est ipsam.');
--11
INSERT INTO `post` (`id`, `heading`, `created_at`, `body`) VALUES (11, 'Quo querat', '2014-07-13', 'Ea et repellat quae non est. Doloremque vel qui qui consequatur placeat pariatur labore enim. Fugiat enim tempore autem excepturi ut id occaecati suscipit. Est molestias consequatur et reiciendis beatae delectus magni sit.
Asperiores debitis provident est. Id quo est quos. Officia deserunt veniam velit reiciendis sequi omnis natus. Incidunt saepe id commodi. Quos voluptatem sint velit ex a dolorem neque. Dolores quas culpa nemo non cumque iste nisi.
Quae eum molestiae facilis id. Voluptatem neque minima assumenda et ut quo quaerat. Aut culpa delectus et sunt consequuntur.
Optio similique dolores maiores atque optio. Est libero maiores vel quis sint est iste molestias. Non ea non a sed quisquam nostrum id et. Unde eum magni reiciendis beatae earum optio. Tempore unde dolore quo quos cupiditate facilis illo. Error et numquam error temporibus in.
Porro iure cumque nihil dolor inventore quia et quia. Itaque quisquam aliquam similique tempora iste saepe explicabo ea. Velit voluptas repellendus sit necessitatibus aut sunt debitis.
Ea ex fuga praesentium esse. Qui magni aut qui et aut nisi quia. Qui non qui repudiandae nostrum consequuntur ratione. Corrupti saepe alias exercitationem.
Omnis voluptate natus aliquam similique aperiam nam temporibus. Dolorum asperiores excepturi at maxime aliquam non fuga quia. Tempore consequatur quaerat quaerat. Non voluptas consectetur rerum dolor neque. Nulla commodi rem corporis consequatur ducimus. Tempore possimus placeat magnam eius.
Illo et aliquid voluptas animi. Ut eaque nostrum et dolorum cum tempore. Debitis enim soluta minima a eos. Laborum unde nisi eum enim qui nemo. Voluptatem neque voluptas nemo quas rerum aliquid placeat.
Recusandae molestiae sunt et aliquid aut quibusdam cupiditate delectus. Rerum optio consequuntur voluptate exercitationem odio. Necessitatibus ullam sit mollitia iure nostrum. Autem non sapiente nostrum quo quasi ut voluptatem sint. Possimus molestias commodi dolorum quidem. Sed est quam eligendi.');
--12
INSERT INTO `post` (`id`, `heading`, `created_at`, `body`) VALUES (12, 'Eius accusamus :)', '2014-10-22', 'Qui id esse ea dolores et perspiciatis. Quos magnam tempore explicabo vero explicabo et. Eius expedita deserunt enim officiis. Rerum nihil nam molestiae. Tenetur ex in sit sit assumenda delectus accusamus asperiores.');
--13
INSERT INTO `post` (`id`, `heading`, `created_at`, `body`) VALUES (13, 'Corrupti neque', '2014-10-26', 'Doloribus dolor fugit iste sequi cumque dolor. Sapiente nisi minus qui est neque temporibus blanditiis. Ratione ut voluptates debitis ipsum exercitationem incidunt.
Maxime dolores omnis assumenda quia impedit corporis. Est aperiam deleniti aut ea enim quia qui. Rerum sit iure suscipit. Corrupti neque explicabo hic atque enim aperiam qui.');
--14
INSERT INTO `post` (`id`, `heading`, `created_at`, `body`) VALUES (14, 'Odit veniam', '2014-11-01', 'Accusantium aliquid officia labore ad quos quaerat. Veniam quod qui tenetur sunt. Odit veniam omnis non sequi et expedita voluptatem ut.
Itaque fugit vero voluptatum magnam vero alias. Cupiditate ab recusandae quis dolores. Suscipit atque eos hic doloribus. Reiciendis provident occaecati ipsum autem. Eos neque ut odio. Non libero doloribus qui laboriosam nesciunt quos non.');
--15
INSERT INTO `post` (`id`, `heading`, `created_at`, `body`) VALUES (15, 'Sed dolore ratione', '2014-11-08', 'Vel facere rem nihil qui. Quia non ut sed tenetur sit qui. Optio aut aspernatur sed dolore ratione non. Corporis aspernatur distinctio recusandae sequi ea. Aut aliquam velit sequi maxime. Temporibus sed culpa illo ab rerum laudantium fugiat.
Ad voluptas molestiae quas nesciunt magni molestiae alias. Totam at dolores quibusdam laudantium et. Nemo impedit porro voluptatem est possimus. Eum repellat aut placeat odit et deleniti reprehenderit.
Nesciunt non deserunt iure. Architecto facere fuga animi eius. Aliquid rerum dolor inventore quas praesentium aut voluptatum blanditiis. Laborum beatae qui qui. Possimus exercitationem eos labore qui.');
--16
INSERT INTO `post` (`id`, `heading`, `created_at`, `body`) VALUES (16, 'Et est et', '2014-11-15', 'Aliquid sed similique eligendi. Eos a ut consequatur. Qui tenetur et est et quos aliquam voluptas. Voluptates magni odit accusantium veniam ab veniam nihil ut. Recusandae facere soluta ut hic ut et rerum similique. Voluptatem aspernatur rerum omnis sit ducimus deserunt sunt.');

--Comments for 16
INSERT INTO `comment` (`post_id`, `author`, `created_at`, `body`) VALUES (16, 'Romulus', '2014-11-16', 'Certe? Malum scriptum sit.');
INSERT INTO `comment` (`post_id`, `author`, `created_at`, `body`) VALUES (16, 'mora', '2014-11-18', '@romulus ab aula discedere, placeo');
