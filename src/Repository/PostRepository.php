<?php

namespace App\Repository;

use App\Entity\Post;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Post|null find($id, $lockMode = null, $lockVersion = null)
 * @method Post|null findOneBy(array $criteria, array $orderBy = null)
 * @method Post[]    findAll()
 * @method Post[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method Post[]    findPage(int $page)
 * @method int       countPages()
 */
class PostRepository extends ServiceEntityRepository
{
    const PAGE_SIZE = 5;

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Post::class);
    }

    public function findPage($page)
    {
        $offset = (($page - 1) * self::PAGE_SIZE);

        return $this->createQueryBuilder('p')
            ->orderBy('p.createdAt', 'DESC')
            ->setMaxResults(self::PAGE_SIZE)
            ->setFirstResult($offset)
            ->getQuery()
            ->getResult();
    }

    public function countPages(): int
    {
        return ceil($this->createQueryBuilder('p')
            ->select('COUNT(p.id)')
            ->getQuery()
            ->getSingleScalarResult() / self::PAGE_SIZE);
    }
}
