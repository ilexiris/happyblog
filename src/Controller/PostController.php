<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use App\Entity\Post;
use App\Entity\Comment;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class PostController extends Controller
{
    public function list($page)
    {
        $posts = $this->getDoctrine()->getRepository(Post::class)->findPage($page);
        $pageCount = $this->getDoctrine()->getRepository(Post::class)->countPages();

        return $this->render('post/list.html.twig', [
            'posts' => $posts,
            'currentPage' => $page,
            'pageNumbers' => range(1, $pageCount),
        ]);
    }

    public function view($id)
    {
        $commentForm = $this->createFormBuilder(null)
            ->setAction($this->generateUrl('app_comment_create'))
            ->setMethod('POST')
            ->add('author', TextType::class, ['label' => 'Your name'])
            ->add('body', TextareaType::class, ['label' => 'Message'])
            ->add('id', HiddenType::class, ['data' => $id])
            ->add('save', SubmitType::class, ['label' => 'Post comment'])
            ->getForm()
            ->createView();

        $post = $this->getDoctrine()->getRepository(Post::class)->find($id);
        // Not sure how to get `order by' into default hydration, so we're doing this for now.
        $comments = $this->getDoctrine()->getRepository(Comment::class)
            ->findBy(['post' => $id], ['createdAt' => 'DESC']);
        return $this->render('post/view.html.twig', [
            'post' => $post,
            'comments' => $comments,
            'commentForm' => $commentForm,
        ]);
    }
}
