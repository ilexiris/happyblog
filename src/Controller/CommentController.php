<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Post;
use App\Entity\Comment;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\ORM\EntityManagerInterface;

class CommentController extends Controller
{
    // TODO: Use a form object both for creating the form and saving its content here.
    public function create(EntityManagerInterface $em, Request $request)
    {
        $author = $request->request->get('form')['author'];
        $body = $request->request->get('form')['body'];
        $postId = $request->request->get('form')['id'];

        $correspondingPost = $em->getRepository(Post::class)->find($postId);

        $comment = new Comment();
        $comment
            ->setAuthor($author)
            ->setBody($body)
            ->setPost($correspondingPost)
            ->setCreatedAt(new \Datetime());

        $em->persist($comment);
        $em->flush();

        return $this->redirectToRoute('app_post_view', ['id' => $postId]);
    }
}
